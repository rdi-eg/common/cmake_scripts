include(${CMAKE_CURRENT_LIST_DIR}/utils.cmake)

# downloads a model from the ftp
# @param remote_models_path the path of the model on the ftp server
# @param local_models_folder the destination path the model will be placed in
# @param a variable we will place the path of the downloaded models into to be used outside of this file 
# (consider it a variable passed by reference )
# TODO: delete me when everything move to the new models structure.
function(rdi_get_models remote_models_path local_models_folder return_models_path)
    
    if(UNIX)
        set(os linux)
    else()
        set(os windows)
    endif()
    if(NOT EXISTS ${local_models_folder})
        make_directory(${local_models_folder})
    endif()	
        
    get_filename_component(models_name ${remote_models_path} NAME)
    set(local_models_path ${local_models_folder}/${models_name}/)

    if(NOT DEFINED mounted_ftp)
        if(NOT EXISTS ${local_models_path})
            file(MAKE_DIRECTORY ${local_models_path})
        
            message(STATUS "Downloading ${models_name} models from turtle --")
            
            execute_process(
                COMMAND
                    scp -r ${remote_models_path}/* ${local_models_path}
                RESULT_VARIABLE
                    return_status
            )
            
            if(NOT ${return_status} EQUAL 0)
                message(FATAL_ERROR "scp failed")
            endif()
        else()
            message(STATUS "Skipping ${models_name} models. Directory already exists.")
        endif()
    else()
        string(REPLACE "models/ocr" "ocr/models" local_models_path ${local_models_path})
    endif()

    set(${return_models_path} ${local_models_path} PARENT_SCOPE)
    
endfunction()

# @brief get the lastest version of the given model.
# @param ftp_model_path -> is the path to model on the ftp server where all versions exist.
# @return lastest_version -> return variable holds the latest version
function(get_latest_model_version ftp_model_path lastest_version)
    execute_process(COMMAND ssh ninja@turtle.rdi ls -1v ${ftp_model_path}
                    OUTPUT_VARIABLE result
                    ERROR_VARIABLE  error
                    RESULT_VARIABLE status
                    COMMAND_ECHO STDOUT
                    )
    if(status)
        message(${error})
        message(FATAL_ERROR "-- Can't get the lastest version of `${ftp_model_path}` model from the ftp.")
    endif()

    # convert string to list.
    string(REGEX REPLACE "\n$" "" MODEL_VERSIONS "${result}")
    string(REPLACE "\n" ";" MODEL_VERSIONS ${MODEL_VERSIONS})

    # get the latest version
    list(LENGTH MODEL_VERSIONS len)
    math(EXPR index "${len} - 1")
    list(GET MODEL_VERSIONS ${index} VERSION)

    set(${lastest_version} ${VERSION} PARENT_SCOPE)

endfunction() 

# @brief get the lastest major version of the given model.
# @param ftp_model_path -> is the path to model on the ftp server where all versions exist.
# @return latest_major_version -> return variable holds the latest major version
# @note we use the same format for reporting code build_number and model major number,
# that is why I call get_build_number to parse the file [confusing] I know.
function(get_latest_model_major_version ftp_model_path latest_major_version)
	get_build_number (${ftp_model_path} number)
	set(${latest_major_version} ${number} PARENT_SCOPE)
endfunction() 


### @brief: download the a specifc version of a model from the ftp and return local full path to the model.
### @param: version -> the version of the model.
### @param: encrypted -> if enabled download the encrypted model.
### @param: ftp_path -> the base path of the model on the ftp.
### @param: local_path -> the base path of the model on the local machine.
### @return: full_local_path -> full local path of the model. ex, ${local_path}/version/model
###
function(download_model version encrypted ftp_path local_path full_local_path)

    # remove forward slash at the end.
    string(REGEX REPLACE "/$" "" ftp_path "${ftp_path}")
    string(REGEX REPLACE "/$" "" local_path "${local_path}")

    # Download the model
    set(ftp_path 	${ftp_path}/${version})
    set(local_path 	${local_path}/${version})

    # set the full paths of the model locally and remotely.
    if(encrypted)
        set(ftp_path 	${ftp_path}/model_enc)
        set(local_path 	${local_path}/model_enc)
    else()
        set(ftp_path 	${ftp_path}/model)
        set(local_path 	${local_path}/model)
    endif()

    set(${full_local_path} ${local_path} PARENT_SCOPE)

    # download the model from the ftp.
    download_dir_from_ftp(${ftp_path} ${local_path})

endfunction()

### @brief: download the GT of of the given model from the ftp and return local full path to the GT.
### @param: version -> the version of the model.
### @param: ftp_path -> the base path of the model on the ftp.
### @param: local_path -> the base path of the model on the local machine.
### @return: full_local_path -> full local path of the model. ex, ${local_path}/version/model
###
function(download_model_GT_sample version ftp_path local_path full_local_path)

    # remove forward slash at the end.
    string(REGEX REPLACE "/$" "" ftp_path "${ftp_path}")
    string(REGEX REPLACE "/$" "" local_path "${local_path}")

    # Download the model
    set(ftp_path ${ftp_path}/${version}/gt_sample)
    set(local_path ${local_path}/${version}/gt_sample)
    
    set(${full_local_path} ${local_path} PARENT_SCOPE)

    # download the model GT sample from the ftp.
    download_dir_from_ftp(${ftp_path} ${local_path})
    
endfunction()

### @brief: generate gt_samples from GT directory from the ftp.
### @param: remote_GT_path -> the path on the ftp server.
### @param: local_gt_sample_path -> the path on your local machine that it be the gt_sample folder.
function(generate_gt_samples remote_GT_path local_gt_sample_path)
    set(orig_remote_GT_path "${remote_GT_path}")
	set(remote_GT_path "ninja@turtle.rdi:${remote_GT_path}")
	
    if(NOT EXISTS ${local_gt_sample_path})
        file(MAKE_DIRECTORY ${local_gt_sample_path})
    else()
        message("-- ${local_gt_sample_path} Already Exists ---")
        return()
    endif()

    message("-- Download ${files_count} from ${remote_GT_path}  ---")

    file(MAKE_DIRECTORY ${local_gt_sample_path}/inputs)
    file(MAKE_DIRECTORY ${local_gt_sample_path}/outputs)

    # download inputs sample file
    execute_process(
        COMMAND ssh ninja@turtle.rdi "cat ${orig_remote_GT_path}/inputs/samples"
        RESULT_VARIABLE return_status
        OUTPUT_VARIABLE input_samples
        COMMAND_ECHO STDOUT
    )
    if(NOT ${return_status} EQUAL 0)
        message(FATAL_ERROR "-- inputs sample file not found ---")
    endif()

    # download outputs sample file
    execute_process(
        COMMAND ssh ninja@turtle.rdi "cat ${orig_remote_GT_path}/outputs/samples"
        RESULT_VARIABLE return_status
        OUTPUT_VARIABLE output_samples
        COMMAND_ECHO STDOUT
    )
    if(NOT ${return_status} EQUAL 0)
        message(FATAL_ERROR "-- outputs sample file not found ---")
    endif()

    # download input samples
    string(STRIP "${input_samples}" input_samples)
    string(REPLACE " " ";" input_samples ${input_samples})
    foreach(input ${input_samples})
        message("-- Downloading " ${input} " ...")
        execute_process(
            COMMAND scp ${remote_GT_path}/inputs/${input} ${local_gt_sample_path}/inputs
            RESULT_VARIABLE return_status
            OUTPUT_QUIET
        )
        if(NOT ${return_status} EQUAL 0)
            message(FATAL_ERROR "-- Downloading inputs samples Fails ---")
        endif()
    endforeach()

    # download output samples
    string(STRIP "${output_samples}" output_samples)
    string(REPLACE " " ";" output_samples ${output_samples})
    foreach(output ${output_samples})
        message("-- Downloading " ${output} " ...")
        execute_process(
            COMMAND scp ${remote_GT_path}/outputs/${output} ${local_gt_sample_path}/outputs
            RESULT_VARIABLE return_status
            OUTPUT_QUIET
        )
        if(NOT ${return_status} EQUAL 0)
            message(FATAL_ERROR "-- Downloading outputs samples Fails ---")
        endif()
    endforeach()
endfunction()