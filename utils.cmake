### @brief: get the source directory of a target
### @param: target -> the target
### @param: dir -> the returned target directory

function(get_target_dir target return_dir)
    get_target_property(target_type ${target} TYPE)

    if(target_type STREQUAL "INTERFACE_LIBRARY")
        get_target_property(target_srcs ${target} INTERFACE_SOURCES)
        list(GET target_srcs 0 target_src)
        get_filename_component(target_dir ${target_src} DIRECTORY)
    else()
        get_target_property(target_dir target SOURCE_DIR)
    endif()
    
    set(${return_dir} ${target_dir} PARENT_SCOPE)
endfunction()

### @brief: download a directory from the ftp.
### @param: remote_path -> the path on the ftp server.
### @param: local_path -> the path on your local machine.
function(download_dir_from_ftp remote_path local_path)

	set(remote_path "ninja@turtle.rdi:${remote_path}")
	
    if(NOT EXISTS ${local_path})
        file(MAKE_DIRECTORY ${local_path})
    else()
        message("-- ${local_path} Already Exists ---")
        return()
    endif()

    message("-- Download ${remote_path}  ---")
    execute_process(
        COMMAND scp -rq ${remote_path}/* ${local_path}
        RESULT_VARIABLE return_status
        COMMAND_ECHO STDOUT
    )

    if(NOT ${return_status} EQUAL 0)
        message(FATAL_ERROR "-- Downloading ${remote_path} Fails ---")
    endif()
endfunction()

### @brief: download a file from the ftp.
### @param: remote_path -> the path on the ftp server.
### @param: local_path -> the path on your local machine.
function(download_file_from_ftp remote_path local_path)

	set(remote_path "ninja@turtle.rdi:${remote_path}")
	
    if(NOT EXISTS ${local_path})
        file(MAKE_DIRECTORY ${local_path})
    endif()

    message("-- Download ${remote_path}  ---")
    execute_process(
        COMMAND scp ${remote_path} ${local_path}
        RESULT_VARIABLE return_status
        COMMAND_ECHO STDOUT
    )

    if(NOT ${return_status} EQUAL 0)
        message(FATAL_ERROR "-- Downloading ${remote_path} Fails ---")
    endif()
endfunction()

# @brief: get the current build number
# @param build_number_path: ftp path to the build number file.
function(get_build_number build_number_path build_number)
execute_process(
        COMMAND scp ninja@turtle.rdi:${build_number_path} ${CMAKE_BINARY_DIR}
        RESULT_VARIABLE status
        COMMAND_ECHO STDOUT
	)
	
	if(NOT ${status} EQUAL 0)
        message(FATAL_ERROR "-- Failing to get build version from ${build_number_path} ---")
	endif()

	get_filename_component(file_name ${build_number_path} NAME)
	message("Path ${file_name}")

	file (STRINGS  "${CMAKE_BINARY_DIR}/${file_name}" BUILDNUM)
	set(${build_number} ${BUILDNUM} PARENT_SCOPE)

endfunction()


function(get_new_build_number build_number_path new_build_number)
	get_build_number (${build_number_path} build_number)
	MATH(EXPR build_number "${build_number}+1")

	set(new_build_number ${build_number} PARENT_SCOPE)
endfunction()


# @brief: install the shared libraries beside your target.
# @param target: your target name.
# @param third_party: the name of your 3rdparty in the prebuilt on the ftp.
# @param pattern: the shared libs. It can be *.so in linux, or *.dll in windows, or just one so like libc.so.
function(install_3rdparty_libs_v2 target third_party pattern)

    set(prefix "${workspace_location}/@rdi/3rdparties")
    if(UNIX)
        file(GLOB THIRD_PARTY_LIBS "${prefix}/linux/${third_party}/release/${pattern}")
    elseif(WIN32)
        file(GLOB THIRD_PARTY_LIBS "${prefix}/windows/${third_party}/release/${pattern}")
    endif()

    add_custom_command(TARGET ${target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${THIRD_PARTY_LIBS} $<TARGET_FILE_DIR:${target}>
    )

endfunction()

# @brief: install the shared libraries beside your target.
# @param target: your target name.
# @param third_party: the name of your 3rdparty in the prebuilt on the ftp.
function(install_3rdparty_libs target third_party)
    set(pattern "")
    if(UNIX)
		set(pattern "*.so*")
    elseif(WIN32)
		set(pattern "*.dll")
    endif()

    install_3rdparty_libs_v2(${target} ${third_party} ${pattern})

endfunction()

# @brief: install the cpp runtime dlls on windows.
# @param target: your target name.
function(install_windows_cpp_runtime_dlls target)
	set(cpp_runtime_dlls ninja@turtle.rdi:/home/ninja/cpp_team/deploy_requirements/windows)

	add_custom_command( TARGET ${target} POST_BUILD 
		COMMAND ${CMAKE_COMMAND} -E env PATH="C:/Windows/System32/OpenSSH/"
			  	scp -r ${cpp_runtime_dlls}/*.dll $<TARGET_FILE_DIR:${target}>
	)
endfunction()

# @brief: install the cpp runtime dlls on windows.
# @param target: your target name.
function(install_windows_cpp_runtime_dlls_v2 target)
	set(cpp_runtime_dlls ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/windows/crt/releasex)

	add_custom_command( TARGET ${target} POST_BUILD 
		COMMAND ${CMAKE_COMMAND} -E env PATH="C:/Windows/System32/OpenSSH/"
			  	scp -r ${cpp_runtime_dlls}/*.dll $<TARGET_FILE_DIR:${target}>
	)
endfunction()
