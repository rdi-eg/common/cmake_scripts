### @brief: enable unit_counter for a target
### @param: target -> the target

include(${CMAKE_CURRENT_LIST_DIR}/rdi_external_project_add.cmake)

function(enable_unit_counter target)
    message(STATUS "UNIT_COUNTER IS ENABLED")

    target_compile_definitions(${target} PRIVATE UNIT_COUNTER)

    rdi_external_project_add(rdi_unit_counter
        GIT_REPO   "git@arwash.rdi:common/unit_counter.git"
        PATH       ${workspace_location}/common/unit_counter
        GIT_TAG    master
    )

    target_link_libraries(${target} PRIVATE rdi_unit_counter)
endfunction()