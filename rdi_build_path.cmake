# Takes the source_directory and returns where the build files
# should be depending on whether you're on windows/linux
# and whether you're doing a debug or release build
# @param source_path[in]
# @param build_path[out]
# @return nothing
function(rdi_build_path source_path build_path)
	if(WIN32)
		set(${build_path} ${source_path}/build/windows PARENT_SCOPE)
		return()
	endif()

	if(NOT CMAKE_BUILD_TYPE)
		message(FATAL_ERROR "CMAKE_BUILD_TYPE is not defined.")
	endif()

	if(ANDROID_ABI)
		set(${build_path} ${source_path}/build/android_${ANDROID_ABI}/${CMAKE_BUILD_TYPE} PARENT_SCOPE)
	elseif(IOS_ABI)
		set(${build_path} ${source_path}/build/ios_${IOS_ABI}/${CMAKE_BUILD_TYPE} PARENT_SCOPE)
	elseif(APPLE)
		set(${build_path} ${source_path}/build/mac/${CMAKE_BUILD_TYPE} PARENT_SCOPE)
	elseif(UNIX)
		set(${build_path} ${source_path}/build/linux/${CMAKE_BUILD_TYPE} PARENT_SCOPE)
	else()
		message(FATAL_ERROR "Unsupported target OS")
	endif()
endfunction()

