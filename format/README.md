# Demo:
![](https://user-images.githubusercontent.com/4437447/66123312-31ec3500-e5d1-11e9-8404-492b8eff8511.gif)

# Description:

this module is taken from https://github.com/TheLartians/Format.cmake with small modifications to fit our needs.

it copies the `.clang-format` and `.cmake-format` files to your repo and format your code based on the settings in this file.

for `.clang-format` settings visit https://clang.llvm.org/docs/ClangFormatStyleOptions.html

for `.cmake-format` settings visit https://cmake-format.readthedocs.io/en/latest/configuration.html

this is to make sure all our projects use the same code formatting settings and make it easier to follow our code conventions

# prerequisites:
- Python 2.7 or Python 3
- clang-format
- cmake-format
- git
- cmake

# Usage:
just add this line to your CMakeLists.txt

    add_subdirectory(${cmake_scripts_path}/format)

to format c++ code and cmake files in your project

    cmake --build . --target fix-format

to format c++ code only

    cmake --build . --target fix-clang-format

to format cmake files only

    cmake --build . --target fix-cmake-format

to check petential changes after formatting without apllying them

    cmake --build . --target format


for more info go to https://github.com/TheLartians/Format.cmake

