# get_os sets the variable os to the current system os
# os  global variable 
message("IN cmake_scrip")
function(get_os)
    if(ANDROID_ABI)
            set(os android_${ANDROID_ABI} PARENT_SCOPE)
    elseif(IOS_ABI)
            set(os ios_${IOS_ABI} PARENT_SCOPE)
    elseif(UNIX AND APPLE)
            set(os mac PARENT_SCOPE)
    elseif(UNIX)
            set(os linux PARENT_SCOPE)
    elseif(WIN32)
            set(os windows PARENT_SCOPE)
    else()
            message(FATAL_ERROR "Unsupported target OS")
    endif()
endfunction()

function(get_host_os)
    if(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Darwin")
            set(os mac PARENT_SCOPE)
    elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Linux")
            set(os linux PARENT_SCOPE)
    elseif(${CMAKE_HOST_SYSTEM_NAME} STREQUAL "Windows")
            set(os windows PARENT_SCOPE)
    else()
            message(FATAL_ERROR "Unrecognized host OS")
    endif()
endfunction()


# rdi_prebuilt_include fetches header files of a specific library from ftp and places is at 
# workspace_location/@rdi/3rdparties/${os}/${lib_name}/include
# @param target_name the name of the lib/executable u want the includes to be linked with
# @param visibility a cmake variable that is used to set the scope of the includes to be seen by the target only(PRIVATE)
# or by other targets that link with ur target(PUBLIC)
# @param lib_name the name of the lib u want the includes from which is located in the ftp server
# @param workspace_location the path to the workspace , this is used to place the includes in.
# @return nothing
function(rdi_prebuilt_include target_name visibility lib_name workspace_location)

	get_os()
	
	if(NOT DEFINED mounted_ftp)
		set(thirdparties_dir ${workspace_location}/@rdi/3rdparties/${os})
		set(include_path ${thirdparties_dir}/${lib_name}/include)

		# To create a file to indicate successful download
		set(include_path_success ${include_path}/success)
		set(remote_include_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/include)

		# Does the success file exist?
		if(NOT EXISTS ${include_path_success})
			file(MAKE_DIRECTORY ${include_path})

			message(STATUS "Downloading ${lib_name} headers from turtle :D")

			set(remote_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${lib_name}/include)

			execute_process(
				COMMAND
					scp -r ${remote_include_path}/* ${include_path}
				RESULT_VARIABLE
					return_status
			)

			if(${return_status} EQUAL 0)
				# Creates the success file
				file(WRITE ${include_path_success} "includes has been downloaded successfully")
			else()
				message(FATAL_ERROR "scp failed")
			endif()
		else()
			message(STATUS "Skipping ${lib_name} headers. Directory already exists.")
		endif()
	else()
		set(thirdparties_dir ${workspace_location}/@rdi/prebuilt/${os})
		set(include_path ${thirdparties_dir}/${lib_name}/include)
	endif()

	target_include_directories(${target_name} ${visibility} ${include_path})

endfunction()

macro(__rdi_prebuilt_download_folder folder_name workspace_location folder_path)
    if(NOT DEFINED mounted_ftp)
            set(local_path ${workspace_location}/@rdi/3rdparties/${os}/${folder_name})
            set(remote_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${folder_name})
            # To create a file to indicate successful download
            set(success_file_path ${local_path}/success)

            # Does the success file exist? if not download the folder.
            if(NOT EXISTS ${success_file_path})
                    file(MAKE_DIRECTORY ${local_path})
                    message(STATUS "Downloading ${folder_name} folder from turtle :D")

                    execute_process(
                            COMMAND scp -r ${remote_path}/* ${local_path}
                            RESULT_VARIABLE return_status
                    )

                    if(${return_status} EQUAL 0)
                            file(WRITE ${success_file_path} "downloaded successfully")
                    else()
                            message(FATAL_ERROR "scp failed to download ${folder_name} from the ftp.")
                    endif()

            else()
                    message(STATUS "Skipping ${folder_name} folder already exist.")
            endif()
    else()
            set(local_path ${workspace_location}/@rdi/prebuilt/${os}/${folder_name})
    endif()

    set(${folder_path} ${local_path} PARENT_SCOPE)
endmacro()

# rdi_prebuilt_download_folder download folder from the prebuilt folder in ftp
# @param folder_name: the name of the folder in the ftp
# @param workspace_location the path to the workspace , this is used to place the includes in.
# @return folder_path: the local path of the folder
function(rdi_prebuilt_download_folder folder_name workspace_location folder_path)
	message("rrrrrr ${workspace_location}")
    get_os()
    __rdi_prebuilt_download_folder(${folder_name} ${workspace_location} ${folder_path})
endfunction()

function(rdi_prebuilt_download_folder_for_host_os folder_name workspace_location folder_path)
    get_host_os()
    __rdi_prebuilt_download_folder(${folder_name} ${workspace_location} ${folder_path})
endfunction()


# rdi_prebuilt_include fetches lib files of a specific library from ftp and places is at 
# workspace_location/@rdi/3rdparties/${os}/{lib_name}/release
# @param target_name the name of the lib/executable u want the libs to be linked with
# @param lib_name the name of the lib u want the libs from which is located in the ftp server
# @param workspace_location the path to the workspace , this is used to place the libraries in.
# @return nothing
function(rdi_prebuilt_libraries target_name lib_name workspace_location)

	get_os()

	if(NOT DEFINED mounted_ftp)
		set(thirdparties_dir ${workspace_location}/@rdi/3rdparties/${os})
		set(lib_path ${thirdparties_dir}/${lib_name}/release)

		# To create a file to indicate successful download
		set(lib_path_success ${lib_path}/success)
		set(remote_lib_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/release)

		# Does the success file exist?
		if(NOT EXISTS ${lib_path_success})
			file(MAKE_DIRECTORY ${lib_path})

			message(STATUS "Downloading ${lib_name} libs from turtle :D")

			execute_process(
				COMMAND
					scp -r ${remote_lib_path}/* ${lib_path}
				RESULT_VARIABLE
					return_status
			)


			if(${return_status} EQUAL 0)
				# Creates the success file
				file(WRITE ${lib_path_success} "libs has been downloaded successfully")
			else()
				message(FATAL_ERROR "scp failed")
			endif()
		else()
			message(STATUS "Skipping ${lib_name} libs. Directory already exists.")
		endif()
	else()
		set(thirdparties_dir ${workspace_location}/@rdi/prebuilt/${os})
		set(lib_path ${thirdparties_dir}/${lib_name}/release)
	endif()

	set(oneValueArgs "")
	set(multiValueArgs PRIVATE INTERFACE PUBLIC)
	set(options "")

	cmake_parse_arguments(RPL
		"${options}"
		"${oneValueArgs}"
		"${multiValueArgs}"
		${ARGN}
	)

	foreach(i ${RPL_PRIVATE})
		list(APPEND privates ${lib_path}/${i})
	endforeach()

	foreach(i ${RPL_INTERFACE})
		list(APPEND interfaces ${lib_path}/${i})
	endforeach()

	foreach(i ${RPL_PUBLIC})
		list(APPEND publics ${lib_path}/${i})
	endforeach()

	if(privates)
		target_link_libraries(${target_name} PRIVATE ${privates}) # ( ͡° ͜ʖ ͡°)
	endif()

	if(interfaces)
		target_link_libraries(${target_name} INTERFACE ${interfaces})
	endif()

	if(publics)
		target_link_libraries(${target_name} PUBLIC ${publics})
	endif()

endfunction()

# Will help you download a prebuilt binary from ninja
# Please use only if the binary is needed in the build process
# will return the directory where the binary is downloaded in bin_dir
# Example usage:
# rdi_prebuilt_binary("markdown" ${workspace_location} bin_dir)
# @param bin_name the binary name you want from ftp
# @param  workspace_location the path to the workspace
# @param bin_dir a variable used to garantee that the binary download was successfull
# @return nothing
function(rdi_prebuilt_binary bin_name workspace_location bin_dir)

	if(NOT bin_dir)
		message(FATAL_ERROR "you must pass a variable name in bin_dir.
			This is how you know where the binary is downloaded.")
	endif()

  get_os()

	if(NOT DEFINED mounted_ftp)
		set(thirdparties_dir ${workspace_location}/@rdi/3rdparties/${os})
		set(bin_path ${thirdparties_dir}/${bin_name}/)
		# To create a file to indicate successful download
		set(bin_path_success ${bin_path}/success)
		set(remote_bin_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${bin_name}/${bin_name})

		# Does the success file exist?
		if(NOT EXISTS ${bin_path_success})
			file(MAKE_DIRECTORY ${bin_path})

			message(STATUS "Downloading ${bin_name} from turtle :D")

			execute_process(
				COMMAND
					scp -r ${remote_bin_path} ${bin_path}
				RESULT_VARIABLE
					return_status
			)

			if(${return_status} EQUAL 0)
				# Creates the success file
				file(WRITE ${bin_path_success} "has been downloaded successfully")
			else()
				message(FATAL_ERROR "scp failed")
			endif()
		else()
			message(STATUS "Skipping ${bin_name}. Already exists.")
		endif()
	else()
		set(thirdparties_dir ${workspace_location}/@rdi/prebuilt/${os})
		set(bin_path ${thirdparties_dir}/${bin_name}/)
	endif()

	set(${bin_dir} ${bin_path} PARENT_SCOPE)

endfunction()


# rdi_prebuilt_runtime fetches dll files of a specific library from ftp will be fetched after ur target has been built
# @param target_name the name of the lib/executable u want the dlls to be placed with
# @param lib_name the name of the lib u want the dlls from which is located in the ftp server
# @return nothing
function(rdi_prebuilt_runtime target_name lib_name)
	if(UNIX)
		set(os linux)
		set(touch_command touch)

		set(remote_lib_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/release)

		if(NOT DEFINED mounted_ftp)
			# so will be downloaded only once when file (so_copied_successfully) exists
			add_custom_command(
				OUTPUT so_copied_successfully_${lib_name}_${target_name}
				COMMAND  scp -r ${remote_lib_path}/* $<TARGET_FILE_DIR:${target_name}>
				COMMENT "Getting ${lib_name} .so(s) from turtle...")
		endif()

		add_custom_target(
			copy_so_of_${lib_name}_for_${target_name} ALL
			DEPENDS so_copied_successfully_${lib_name}_${target_name}
		)
	else()
		set(os windows)
		set(touch_command touch)

		set(remote_lib_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/release)

		if(NOT DEFINED mounted_ftp)
			# dlls will be downloaded only once when file (dlls_copied_successfully) exists
			add_custom_command(
				OUTPUT dlls_copied_successfully_${lib_name}_${target_name}
				COMMAND ${CMAKE_COMMAND} -E env PATH="C:/Windows/System32/OpenSSH/"
				scp -r ${remote_lib_path}/*.dll $<TARGET_FILE_DIR:${target_name}>
				COMMENT "Getting ${lib_name} dlls from turtle...")
		endif()

		add_custom_target(
			copy_dlls_of_${lib_name}_for_${target_name} ALL
			DEPENDS dlls_copied_successfully_${lib_name}_${target_name}
		)
	endif()
endfunction()


# rdi_prebuilt_runtime fetches dll files of a specific library from ftp will be fetched during cmake configuration
# this is used during windows build only
# @param lib_name the name of the lib u want the dlls from which is located in the ftp server
# @return nothing
function(rdi_prebuilt_runtime_configtime lib_name output_path)
	if(UNIX)
		message(FATAL "linux is not supported in rdi_prebuilt_runtime()")
		set(touch_command touch)
	else()
		set(os windows)
		set(touch_command touch)
	endif()

	set(remote_lib_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/release)


	# dlls will be downloaded only once when file (dlls_copied_successfully) exists
	execute_process(
		COMMAND scp -r ${remote_lib_path}/*.dll ${output_path}
	)
endfunction()

# rdi_prebuilt_wholearchive same as rdi_prebuilt_library but adds the whole archive option to avoid 
# dependency issues
# @param lib_name the name of the lib u want the libraires from which is located in the ftp server
# @param target_name the name of the lib/executable u want the libs to be linked with
# @param  workspace_location the path to the workspace
function(rdi_prebuilt_wholearchive target_name lib_name workspace_location)

	get_os()

	if(NOT DEFINED mounted_ftp)
		set(remote_lib_path ninja@turtle.rdi:/home/ninja/cpp_team/prebuilt/${os}/${lib_name}/release)
		set(thirdparties_dir ${workspace_location}/@rdi/3rdparties/${os})
		set(lib_path ${thirdparties_dir}/${lib_name}/release)

		if(NOT EXISTS ${lib_path})
			file(MAKE_DIRECTORY ${lib_path})

			message(STATUS "Downloading ${lib_name} libs from turtle :D")

			execute_process(
				COMMAND
					scp -r ${remote_lib_path}/* ${lib_path}
				RESULT_VARIABLE
					return_status
			)

			if(NOT ${return_status} EQUAL 0)
				message(FATAL_ERROR "scp failed")
			endif()

		else()
			message(STATUS "Skipping ${lib_name} libs. Directory already exists.")
		endif()
	else()
		set(thirdparties_dir ${workspace_location}/@rdi/prebuilt/${os})
		set(lib_path ${thirdparties_dir}/${lib_name}/release)
	endif()

	set(oneValueArgs "")
	set(multiValueArgs PRIVATE INTERFACE PUBLIC)
	set(options "")

	cmake_parse_arguments(RPL
		"${options}"
		"${oneValueArgs}"
		"${multiValueArgs}"
		${ARGN}
	)

	foreach(i ${RPL_PRIVATE})
		list(APPEND privates "/WHOLEARCHIVE:${lib_path}/${i}")
	endforeach()

	foreach(i ${RPL_INTERFACE})
		list(APPEND interfaces "/WHOLEARCHIVE:${lib_path}/${i}")
	endforeach()

	foreach(i ${RPL_PUBLIC})
		list(APPEND publics "/WHOLEARCHIVE:${lib_path}/${i}")
	endforeach()

	if(privates)
		target_link_options(${target_name}
			PRIVATE
				${privates}
		)
		#set_target_properties(${target_name} PROPERTIES LINK_FLAGS ${privates})
	endif()

	if(interfaces)
		target_link_options(${target_name}
				INTERFACE
					${interfaces}
			)
	endif()

	if(publics)
		target_link_options(${target_name}
				PUBLIC
					${publics}
			)
	endif()

endfunction()
