add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/format)
include(${CMAKE_CURRENT_LIST_DIR}/tools/CompilerWarnings.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/tools/StaticAnalyzers.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/tools/Sanitizers.cmake)
