# Will add an external project as a subdirectory to your project.
# Will clone the project if it doesn't exist using the provided URL.
# Any targets defined in that project can then be used in your own project.
#
# Arguments:
#	GIT_REPO   A url to a git repo so that the project
#              Will be cloned if it doesn't exist
#	GIT_TAG    (Optional) Specify a branch/tag
#PATH       Where the repo will be cloned
#	PREFIX     The subdirectory in the repo that will be added as a
#              subdirectory. default value is `lib`.
#@return nothing
option(pulldeps "update dependencies by pulling updates from their repositories" OFF)
option(tags "use tagging system" OFF)

function(rdi_external_project_add)

	set(oneValueArgs GIT_REPO GIT_TAG PATH PREFIX)
	set(multiValueArgs "")
	set(options "")

	cmake_parse_arguments(EPA
		"${options}"
		"${oneValueArgs}"
		"${multiValueArgs}"
		${ARGN}
	)

    if(NOT DEFINED EPA_GIT_REPO)
		message(FATAL " GIT_REPO is not defined")
	endif()

	if(NOT DEFINED EPA_PATH)
		message(FATAL " PATH is not defined")
	endif()

	if(NOT DEFINED EPA_PREFIX)
		set(EPA_PREFIX "lib")
	endif()

	# if there's a slash in the end of the path, remove it.
	string(REGEX REPLACE "/$" "" EPA_PATH "${EPA_PATH}")
	string(REGEX REPLACE "/$" "" EPA_PREFIX "${EPA_PREFIX}")

	# make sure we don't add a subdirectory twice in any project
	# sort of like pragma_once
	# pragma_once begin
	set(unique_full_path ${EPA_PATH}/${EPA_PREFIX})
	get_property(unique_id GLOBAL PROPERTY ${unique_full_path})

	if(${unique_id})
		message(STATUS "${unique_full_path} was already included")
		return()
	else()
		set_property(GLOBAL PROPERTY ${unique_full_path} "true")
	endif()
	# pragma_once end

	message(STATUS "including ${unique_full_path}")

	if(NOT EXISTS ${EPA_PATH}/.git)
		if(NOT DEFINED EPA_GIT_TAG)
			set(EPA_GIT_TAG "")
		elseif((NOT tags) AND (EPA_GIT_TAG MATCHES "^v[0-9].[0-9].?[0-9]?$"))
			set(EPA_GIT_TAG "")
		else()
			set(EPA_GIT_TAG "-b;${EPA_GIT_TAG}")
		endif()

		execute_process(
			COMMAND
				git clone ${EPA_GIT_TAG} ${EPA_GIT_REPO} ${EPA_PATH}
			RESULT_VARIABLE
				return_status
		)

		if(NOT ${return_status} EQUAL 0)
			message(FATAL_ERROR "git clone failed")
		endif()
		
	# if the pull deps option is enabled 
	# will git pull from the repo specified by
	elseif(pulldeps)
        execute_process(
			COMMAND
                git -C ${EPA_PATH} pull
			RESULT_VARIABLE
				return_status
		)

		if(NOT ${return_status} EQUAL 0)
			message(FATAL_ERROR "git pull failed")
		endif()
	
	# force checkoutif `EPA_GIT_TAG` is set
	elseif(NOT EPA_GIT_TAG STREQUAL "")
		if(NOT ((NOT tags) AND (EPA_GIT_TAG MATCHES "^v[0-9].[0-9].?[0-9]?$")))
			execute_process(
				COMMAND
					git -C ${EPA_PATH} checkout ${EPA_GIT_TAG}
				RESULT_VARIABLE
					return_status
			)
			if(NOT ${return_status} EQUAL 0)
				message(FATAL_ERROR "git checkout error")
			endif()
		endif()
	endif()

	if(NOT cmake_scripts_path)
		message(FATAL_ERROR "cmake_scripts_path is not defined")
	endif()

	include(${cmake_scripts_path}/rdi_build_path.cmake)

	rdi_build_path(${EPA_PATH} BUILD_PATH)

	execute_process(
		COMMAND
			git -C ${EPA_PATH} rev-parse --short HEAD
		OUTPUT_VARIABLE
			commit_id
	)

	# build dependencies tree and insert it in dependencies.txt
	file(APPEND ${CMAKE_BINARY_DIR}/dependencies.txt "${EPA_GIT_REPO} ${commit_id}")
	
	add_subdirectory(
		${unique_full_path}
		${BUILD_PATH}/${EPA_PREFIX}
	)
	
endfunction()
