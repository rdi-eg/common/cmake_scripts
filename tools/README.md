# brief
some random tools to help detect potential problems in our code

# CompilerWarnings.cmake
enables extra helpful warnings to be produced when compiling with gcc, clang and msvc.
### usage

    include(${cmake_scripts_path}/tools/CompilerWarnings.cmake)
	enable_warnings(target)

# StaticAnalyzers.cmake
enables some static code analysis that display potential problems while compiling code.
### usage
add next line to your cmake after project() function

    include(${cmake_scripts_path}/tools/StaticAnalyzers.cmake)

run the following to turn on/off the static analyzer of your choice

    cmake .. -DENABLE_CPPCHECK=on -DENABLE_CLANG_TIDY=on -DENABLE_INCLUDE_WHAT_YOU_USE=on

# Sanitizers.cmake
enables runtime sanitizers that detect memory leaks, undefined behaviours and threading problems.

they work on specific compilers and some of them don't work together so you need to run them separatly to learn more see [Sanitizers.cmake](Sanitizers.cmake)

### usage
add next line to your cmake after project() function

    include(${cmake_scripts_path}/tools/Sanitizers.cmake)
	enable_sanitizers(target)

run one of the following to turn on/off the sanitizer of your choice ( some sanitizers don't work with each other at the same time)

    cmake .. -DENABLE_SANITIZER_ADDRESS=on
	cmake .. -DENABLE_SANITIZER_LEAK=on
	cmake .. -DENABLE_SANITIZER_UNDEFINED_BEHAVIOR=on
	cmake .. -DENABLE_SANITIZER_THREAD=on
	cmake .. -DENABLE_SANITIZER_MEMORY=on