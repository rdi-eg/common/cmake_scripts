########################################################################
# Always remember to pass unwanted parametes by OFF if you want to neglect it
########################################################################

option(SCRIPT_MODE "you must pass this by true if you ran this file through cmake -P" OFF)
set(MODELS_LOCAL_PATH "" CACHE STRING "the path that the model will be downloaded into")

option(RDI_PROJECT_NAME "project name, default is empty" "")
option(RDI_MODEL_NAME "model name, default is empty" "")
option(MODEL_VERSION "model version, default is empty" "")
option(DOWNLOAD_MODEL "download model only" ON)
option(DOWNLOAD_GT "download GT only" ON)
option(GT_TYPE "GT_TYPE should be one of download_gt_samples, generate_gt_samples, download_whole_gt" OFF)

function(model_manager
    return_model_path
    return_gt_path
)

    set(oneValueArgs 
        RDI_PROJECT_NAME
        RDI_MODEL_NAME
        MODEL_VERSION
        GT_TYPE
        DOWNLOAD_MODEL
        DOWNLOAD_GT
        DOWNLOAD_WHOLE_GT)
    set(multiValueArgs "")
    set(options "")

    cmake_parse_arguments(PARSE_ARGV 2 EPA
        "${options}"
        "${oneValueArgs}"
        "${multiValueArgs}"
    )

    if(NOT EPA_GT_TYPE)
        set(EPA_GT_TYPE "download_gt_samples")
    elseif(NOT ((${EPA_GT_TYPE} STREQUAL "download_gt_samples") OR
           (${EPA_GT_TYPE} STREQUAL "generate_gt_samples") OR
           (${EPA_GT_TYPE} STREQUAL "download_whole_gt")))
           message(FATAL_ERROR " GT_TYPE should be one of download_gt_samples, generate_gt_samples, download_whole_gt")
    endif()

    if(NOT EPA_RDI_MODEL_NAME)
        unset(EPA_RDI_MODEL_NAME)
    endif()

    if(NOT EPA_MODEL_VERSION)
        unset(EPA_MODEL_VERSION)
    endif()

    if(NOT EPA_DOWNLOAD_MODEL)
        unset(EPA_DOWNLOAD_MODEL)
    endif()

    if(NOT EPA_DOWNLOAD_GT)
        unset(EPA_DOWNLOAD_GT)
    endif()

    if(NOT EPA_RDI_PROJECT_NAME)
		message(FATAL_ERROR " RDI_PROJECT_NAME is not defined")
	endif()

    if("${MODELS_LOCAL_PATH}" STREQUAL "")
        if(NOT workspace_location)
            set(MODELS_LOCAL_PATH models/${EPA_RDI_PROJECT_NAME})
        else()
            set(MODELS_LOCAL_PATH ${workspace_location}/@rdi/models/${EPA_RDI_PROJECT_NAME})
        endif()
    else()
        set(MODELS_LOCAL_PATH ${MODELS_LOCAL_PATH}/${EPA_RDI_PROJECT_NAME})
    endif()

    # add project_name
    set(MODELS_LOCAL_PATH ${MODELS_LOCAL_PATH}/${rdi_project_name})

    # check if models_new_structure exists or the old model folder instead
    set(MODELS_FTP_PATH "/home/ninja/cpp_team/${EPA_RDI_PROJECT_NAME}/models_new_structure")
    execute_process(COMMAND ssh ninja@turtle.rdi ls -1v ${MODELS_FTP_PATH}
                        RESULT_VARIABLE status
                        ERROR_QUIET
                        OUTPUT_QUIET
    )
    if(status)
        set(MODELS_FTP_PATH "/home/ninja/cpp_team/${EPA_RDI_PROJECT_NAME}/models")
    endif()

    message("-- You can control downloading model throught DOWNLOAD_MODEL option")
    message("-- You can control downloading gt_sample throught DOWNLOAD_GT option")
    message("-- You can download the encrypted/non_encrypted version through RDI_ENCRYPT option")
    message("-- You can set the model path through ${MODELS_LOCAL_PATH}")

    if(RDI_ENCRYPT)
        message("-- Download the encrypted models.")
    else()
        message("-- Download the non encrypted models.")
    endif()

    # use the helper functions from cmakescripts.
    if(NOT cmake_scripts_path)
        set(cmake_scripts_path "${CMAKE_CURRENT_LIST_DIR}/cmake_scripts")

        if(NOT EXISTS "${cmake_scripts_path}")
            execute_process(COMMAND git clone "git@arwash.rdi:common/cmake_scripts.git"
                            ${cmake_scripts_path} 
                            RESULT_VARIABLE status)
            
        else()
            execute_process(COMMAND git pull  
                            RESULT_VARIABLE status
                            WORKING_DIRECTORY ${cmake_scripts_path})
        endif()

        if(NOT ${status} EQUAL 0)
            message(FATAL_ERROR "git clone/pull cmakescripts failed")
        endif()

    endif()

    include(${cmake_scripts_path}/rdi_get_models.cmake)
    include(${cmake_scripts_path}/utils.cmake)

    ###                     @@@@@@  MODEL @@@@@@@@                   #####
    ###                     ----------------------                   #####

    if(DEFINED EPA_RDI_MODEL_NAME)
        set(MODEL_FTP_PATH   ${MODELS_FTP_PATH}/${EPA_RDI_MODEL_NAME})
        set(MODEL_LOCAL_PATH ${MODELS_LOCAL_PATH}/${EPA_RDI_MODEL_NAME})
    else()
        set(MODEL_FTP_PATH   ${MODELS_FTP_PATH})
        set(MODEL_LOCAL_PATH ${MODELS_LOCAL_PATH})
    endif()

    # message("-- You can set the model version through model_version")
    if(NOT DEFINED EPA_MODEL_VERSION)
        get_latest_model_version(${MODEL_FTP_PATH} EPA_MODEL_VERSION)
        message("-- LATEST VERSION OF ${EPA_RDI_MODEL_NAME}/${EPA_RDI_MODEL_NAME} = ${EPA_MODEL_VERSION}")
    endif()

    # GT sample paths
    set(GT_FTP_PATH             ${MODEL_FTP_PATH}/${EPA_MODEL_VERSION}/GT)
    set(GT_SAMPLES_FTP_PATH     ${MODEL_FTP_PATH}/${EPA_MODEL_VERSION}/gt_samples)
    set(GT_LOCAL_PATH           ${MODEL_LOCAL_PATH}/${EPA_MODEL_VERSION}/gt_sample)
    set(WHOLE_GT_LOCAL_PATH     ${MODEL_LOCAL_PATH}/${EPA_MODEL_VERSION}/GT)

    # model paths
    if(RDI_ENCRYPT)
        set(MODEL_FTP_PATH    ${MODEL_FTP_PATH}/${EPA_MODEL_VERSION}/model_enc)
        set(MODEL_LOCAL_PATH  ${MODEL_LOCAL_PATH}/${EPA_MODEL_VERSION}/model_enc)  
    else()
        set(MODEL_FTP_PATH    ${MODEL_FTP_PATH}/${EPA_MODEL_VERSION}/model)
        set(MODEL_LOCAL_PATH  ${MODEL_LOCAL_PATH}/${EPA_MODEL_VERSION}/model)     
    endif()

    # download the model.
    if(DEFINED EPA_DOWNLOAD_MODEL)
        download_dir_from_ftp(${MODEL_FTP_PATH} ${MODEL_LOCAL_PATH})
    endif()

    # download the gt sample.
    if(${EPA_GT_TYPE} STREQUAL "generate_gt_samples")
        if(DEFINED EPA_DOWNLOAD_GT)
            generate_gt_samples(${GT_FTP_PATH} ${GT_LOCAL_PATH})
        endif()
        set(${return_gt_path} ${GT_LOCAL_PATH} PARENT_SCOPE)
    elseif(${EPA_GT_TYPE} STREQUAL "download_gt_samples")
        if(DEFINED EPA_DOWNLOAD_GT)
            download_dir_from_ftp(${GT_SAMPLES_FTP_PATH} ${GT_LOCAL_PATH})
        endif()
        set(${return_gt_path} ${GT_LOCAL_PATH} PARENT_SCOPE)
    elseif(${EPA_GT_TYPE} STREQUAL "download_whole_gt")
        if(DEFINED EPA_DOWNLOAD_GT)
            download_dir_from_ftp(${GT_FTP_PATH} ${WHOLE_GT_LOCAL_PATH})
        endif()
        set(${return_gt_path} ${WHOLE_GT_LOCAL_PATH} PARENT_SCOPE)
    endif()

    set(${return_model_path} ${MODEL_LOCAL_PATH} PARENT_SCOPE)
endfunction()

if(SCRIPT_MODE)
    model_manager(
        return_model_path
        return_gt_path

        RDI_PROJECT_NAME    ${RDI_PROJECT_NAME}
        RDI_MODEL_NAME      ${RDI_MODEL_NAME}
        MODEL_VERSION       ${MODEL_VERSION}
        DOWNLOAD_MODEL      ${DOWNLOAD_MODEL}
        GT_TYPE             ${GT_TYPE}
        DOWNLOAD_GT         ${DOWNLOAD_GT}
    )
endif()
