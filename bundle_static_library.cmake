function(bundle_static_library tgt_name bundled_tgt_name)
  get_target_property(dep_dir ${tgt_name} BINARY_DIR)
  get_target_property(dep_ar_name ${tgt_name} NAME)
  list(APPEND static_libs ${dep_dir}/${CMAKE_STATIC_LIBRARY_PREFIX}${dep_ar_name}${CMAKE_STATIC_LIBRARY_SUFFIX})

  function(_recursively_collect_dependencies input_target)
    set(_input_link_libraries LINK_LIBRARIES)
    get_target_property(_input_type ${input_target} TYPE)
    if (${_input_type} STREQUAL "INTERFACE_LIBRARY")
      set(_input_link_libraries INTERFACE_LINK_LIBRARIES)
    endif()
    get_target_property(public_dependencies ${input_target} INTERFACE_LINK_LIBRARIES)

    foreach(dependency IN LISTS public_dependencies)
      string(REGEX REPLACE ".+:(.+)>" "\\1" dependency ${dependency})
      if(TARGET ${dependency})
        get_target_property(alias ${dependency} ALIASED_TARGET)
        if (TARGET ${alias})
          set(dependency ${alias})
        endif()
        get_target_property(_type ${dependency} TYPE)
        if (${_type} STREQUAL "STATIC_LIBRARY")
          get_target_property(dep_dir ${dependency} BINARY_DIR)
          get_target_property(dep_ar_name ${dependency} NAME)
          list(APPEND static_libs ${dep_dir}/${CMAKE_STATIC_LIBRARY_PREFIX}${dep_ar_name}${CMAKE_STATIC_LIBRARY_SUFFIX})
        endif()

        get_property(library_already_added
          GLOBAL PROPERTY _${tgt_name}_static_bundle_${dependency})
        if (NOT library_already_added)
          set_property(GLOBAL PROPERTY _${tgt_name}_static_bundle_${dependency} ON)
          _recursively_collect_dependencies(${dependency})
        endif()
      elseif(EXISTS ${dependency} AND ${dependency} MATCHES ".+\\.a$")
        string(REGEX REPLACE ".+:(.+)>" "\\1" dependency ${dependency})
        get_property(library_already_added
          GLOBAL PROPERTY _${tgt_name}_static_bundle_${dependency})
        if(NOT library_already_added)
          message(STATUS "adding ${dependency}")
          set_property(GLOBAL PROPERTY _${tgt_name}_static_bundle_${dependency} ON)
          list(APPEND static_libs ${dependency})
        endif()
      endif()
    endforeach()
    set(static_libs ${static_libs} PARENT_SCOPE)
  endfunction()

  _recursively_collect_dependencies(${tgt_name})

  list(REMOVE_DUPLICATES static_libs)

  set(bundled_tgt_full_name
    ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}${bundled_tgt_name}${CMAKE_STATIC_LIBRARY_SUFFIX})

  message(STATUS "CMAKE_CXX_COMPILER_ID = " ${CMAKE_CXX_COMPILER_ID})
  if(CMAKE_CXX_COMPILER_ID MATCHES "AppleClang")
	# Find (Apple's) libtool.
	execute_process(COMMAND xcrun -sdk ${CMAKE_OSX_SYSROOT} -find libtool
	  OUTPUT_VARIABLE BUILD_LIBTOOL
	  ERROR_QUIET
	  OUTPUT_STRIP_TRAILING_WHITESPACE)
	message(STATUS "Using libtool: ${BUILD_LIBTOOL}")

	add_custom_command(
        COMMAND ${BUILD_LIBTOOL} -static -o ${bundled_tgt_full_name} ${static_libs}
        OUTPUT ${bundled_tgt_full_name}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Bundling ${bundled_tgt_full_name} to ${CMAKE_CURRENT_BINARY_DIR}"
        VERBATIM
    )
  elseif(CMAKE_CXX_COMPILER_ID MATCHES "^(Clang|GNU)$")
    file(WRITE ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar.in
      "CREATE ${bundled_tgt_full_name}\n" )

    foreach(tgt IN LISTS static_libs)
      file(APPEND ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar.in
        "ADDLIB $<TARGET_FILE:${tgt}>\n")
    endforeach()

    file(APPEND ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar.in "SAVE\n")
    file(APPEND ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar.in "END\n")

    file(GENERATE
      OUTPUT ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar
      INPUT ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar.in)

    set(ar_tool ${CMAKE_AR})
    if (CMAKE_INTERPROCEDURAL_OPTIMIZATION)
      set(ar_tool ${CMAKE_CXX_COMPILER_AR})
    endif()

    add_custom_command(
      COMMAND ${ar_tool} -M < ${CMAKE_BINARY_DIR}/${bundled_tgt_name}.ar
      OUTPUT ${bundled_tgt_full_name}
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Bundling ${bundled_tgt_name} to ${CMAKE_CURRENT_BINARY_DIR}"
      VERBATIM)
  elseif(MSVC)
    find_program(lib_tool lib)

    foreach(tgt IN LISTS static_libs)
      list(APPEND static_libs_full_names $<TARGET_FILE:${tgt}>)
    endforeach()

    add_custom_command(
      COMMAND ${lib_tool} /NOLOGO /OUT:${bundled_tgt_full_name} ${static_libs_full_names}
      OUTPUT ${bundled_tgt_full_name}
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Bundling ${bundled_tgt_name} to ${CMAKE_CURRENT_BINARY_DIR}"
      VERBATIM)
  else()
    message(FATAL_ERROR "Unknown bundle scenario!")
  endif()

  message(STATUS "bundled_tgt_full_name = " ${bundled_tgt_full_name})

  set(bundling_target ${tgt_name}_bundling_target)
  add_custom_target(${bundling_target} ALL DEPENDS ${bundled_tgt_full_name})
  add_dependencies(${bundling_target} ${tgt_name})

  add_library(${bundled_tgt_name} STATIC IMPORTED OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR})
  set_target_properties(${bundled_tgt_name}
    PROPERTIES
      IMPORTED_LOCATION ${bundled_tgt_full_name}
      INTERFACE_INCLUDE_DIRECTORIES $<TARGET_PROPERTY:${tgt_name},INTERFACE_INCLUDE_DIRECTORIES>)
  add_dependencies(${bundled_tgt_name} ${bundling_target})

endfunction()
