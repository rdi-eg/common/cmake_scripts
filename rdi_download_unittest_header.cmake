# NOTE: to update catch/doctest just change the `tag` varibale 


# downloads a file and places it in [download_path]
# @param fname the name of the file
# @param ftag tag associated with the file (ex: version)
# @param furl the file url on the web
# @param download_path  path that doctest will be placed in
# @return nothing
function(download_file fname ftag furl download_path)
    if(NOT EXISTS ${download_path})
        message("Downloading ${fname} ...")
        file(DOWNLOAD
            ${furl}
            ${download_path}
            STATUS return_status
        )

        list(GET return_status 0 status)
        if(NOT ${status} EQUAL 0)
            list(GET return_status 1 err_message)
            message(FATAL_ERROR ${err_message})
        endif()

    else()
        message(STATUS "skipping ${fname} already exists")
    endif()
endfunction()

# download catch header from github
# @param download_path path that catch will be placed in 
# @return nothing
function(download_catch download_path)
    set(catch_header "catch.hpp")
    set(catch_tag "v2.11.1")
    set(catch_url "https://raw.githubusercontent.com/catchorg/Catch2/${catch_tag}/single_include/catch2/catch.hpp")
    set(download_path "${download_path}/${catch_header}")
    
    download_file(${catch_header} ${catch_tag} ${catch_url} ${download_path})
endfunction()

# download doctest  header from github
# @param download_path path that doctest will be placed in
# @return nothing 
function(download_doctest download_path)
    set(doctest_header "doctest.h")
    set(doctest_tag "2.3.6")
    set(doctest_url "https://raw.githubusercontent.com/onqtam/doctest/${doctest_tag}/doctest/doctest.h")
    set(download_path "${download_path}/${doctest_header}")
    
    download_file(${doctest_header} ${doctest_tag} ${doctest_url} ${download_path})
endfunction()
